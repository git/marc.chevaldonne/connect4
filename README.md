# connect4

```mermaid
classDiagram
    class Player
    Player : int id
    Player : string name

    class Board
    Board : int nbrow
    Board : int nbcol
    Board : array board
    Board : _checkBoundaries(row, col)
    Board : _removePiece(row, col)
    Board : _insertPiece(row, col, playerId)
    Board : insertPieceIntoColumn(col, playerId)

    class Rules
    Rules : string name
    Rules : int nbrow
    Rules : int nbcol
    Rules : checkBoardSize(board) bool
    Rules : isFull(board) bool
    Rules : _checkHorizontalLines(board, row, col) bool, id, winningCell
    Rules : _checkVerticalLines()
    Rules : _checkDiagonalNWtoSE()
    Rules : _checkDiagonalNetoSW()
    Rules : checkAll()

    Rules ..> Board
```