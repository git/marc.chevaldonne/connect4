from connect4.src.board import Board

class Rules:
	def __init__(self):

		self.nb_row = 6
		self.nb_col = 7

	def check_board_size(self, board):
		""" Checks if the board size is correct.
		Entries:
			self: the Rules class itself
			board -> Board: a board
		Output:
			correct -> Bool: if True, the board size is correct
		"""
		correct = False

		
		if self.nb_col == board.nbcol and self.nb_row == board.nbrow :
			correct = True
		else:
			correct = False
		
		return correct
